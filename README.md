# Whos-that-function

Warning: As this game was created as part of an Education Innovation scholarship @ Universidad Politécnica de Madrid, its main language is Spanish. Thanks to the [Grupo de Innovación Educativa "Pensamiento Matemático"](https://innovacioneducativa.upm.es/pensamientomatematico/) for the oportunity to work on this project.

-----

<img src="https://codeberg.org/FlyingFlamingo/Whos-That-Function/raw/branch/main/screenshots/main_screen.png" width="50%">.
<br>
<br>

Part of the "From Game to Theory" teacher-student colaboration scholarship, this game teaches students the fundamentals of functions and their properties in an interactive way, with a game dynamic similar to the "Who's who" series. It was developed using Godot during the 2020/2021 Academic Year. An HTML5 export can be found [in the releases page](https://codeberg.org/FlyingFlamingo/Whos-That-Function/releases) and it can be played for free [on itch.io](https://flyingflamingo.itch.io/whos-that-function)

## Editing the project

In order to work and/or modify the project, you have to [install Godot Engine first](https://godotengine.org/). Once you have it, simply:

1. Clone this repository: `git clone https://codeberg.org/FlyingFlamingo/Whos-That-Function/`
2. Step into the directory: `cd Whos-That-Function`
3. Open the Godot Engine
4. Scan the "Whos-That-Function" folder (or however you rename the project folder)
5. Run the project (it may take some time to import all assets)


To follow this method, you need to have git installed in your system (`sudo apt install git`). Otherwise, simply [download the source](https://codeberg.org/FlyingFlamingo/Whos-That-Function/archive/main.zip), unzip it, and continue from step 3




