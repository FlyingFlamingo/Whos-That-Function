extends Node

#Inicializo ciertas variables
var presionados = [] #Usada para contar qué funciones he clickado
var mylist = _ready() #Lista de valores para las preguntas
var funcionseleccionada #Función seleccionada para validar
var binarycode #Variable que indica el código respuesta de las preguntas
var preguntassinconsultar = 6 #Contador de puntos
var PuntosPorAttempts = 0 #Usado en la escena Seleccionar
var tiempo = "09:59"  #Contador para el tiempo total
onready var minutos = 9; onready var segundos = 59; onready var timeispositive = true

func _ready():
	randomize()
	var one_number_to_rule_them_all = randi()%28+1
	var mylist = csv2Dict(one_number_to_rule_them_all)
	return mylist
 
func play_music():
	$Music.stream = load("res://sounds/thefunction.ogg")
	$Music.play()
	
func stop_music():
	$Music.stop()

func csv2Dict(number):
	#Hardcoding values might be a bit cutre, but it works
	var dict = {1:[0, "No", "No", "No", "No", "No", "No"],\
	2:[1, "No", "No", "No", "No", "No", "Sí"],\
	3:[3, "No", "No", "No", "No", "Sí", "Sí"],\
	4:[4, "No", "No", "No", "Sí", "No", "No"],\
	5:[6, "No", "No", "No", "Sí", "Sí", "No"],\
	6:[8, "No", "No", "Sí", "No", "No", "No"],\
	7:[9, "No", "No", "Sí", "No", "No", "Sí"],\
	8:[12, "No", "No", "Sí", "Sí", "No", "No"],\
	9:[14, "No", "No", "Sí", "Sí", "Sí", "No"],\
	10:[16, "No", "Sí", "No", "No", "No","No"],\
	11:[20, "No", "Sí", "No", "Sí", "No","No"],\
	12:[22, "No", "Sí", "No", "Sí", "Sí","No"],\
	13:[24, "No", "Sí", "Sí", "No", "No","No"],\
	14:[28, "No", "Sí", "Sí", "Sí", "No","No"],\
	15:[30, "No", "Sí", "Sí", "Sí", "Sí","No"],\
	16:[32, "Sí", "No", "No", "No", "No","No"],\
	17:[33, "Sí", "No", "No", "No", "No","Sí"],\
	18:[38, "Sí", "No", "No", "Sí", "Sí","No"],\
	19:[40, "Sí", "No", "Sí", "No", "No","No"],\
	20:[41, "Sí", "No", "Sí", "No", "No","Sí"],\
	21:[44, "Sí", "No", "Sí", "Sí", "No", "No"],\
	22:[46, "Sí", "No", "Sí", "Sí", "Sí", "No"],\
	23:[48, "Sí", "Sí", "No", "No", "No", "No"],\
	24:[52, "Sí", "Sí", "No", "Sí", "No", "No"],\
	25:[54, "Sí", "Sí", "No", "Sí", "Sí", "No"],\
	26:[56, "Sí", "Sí", "Sí", "No", "No", "No"],\
	27:[60, "Sí", "Sí", "Sí", "Sí", "No", "No"],\
	28:[62, "Sí", "Sí", "Sí", "Sí", "Sí", "No"]}
	return dict[number]

func _on_Timer_timeout():
	if timeispositive == true:
		if segundos > 9:
			tiempo = "0"+str(minutos)+":"+str(segundos)
			segundos = segundos - 1
		if segundos <= 9 and segundos > 0:
			tiempo = "0"+str(minutos)+":0"+str(segundos)
			segundos = segundos - 1
		if segundos == 0 and minutos > 0:
			tiempo = "0"+str(minutos)+":00"
			minutos = minutos - 1
			segundos = 59
		if segundos == 0 and minutos == 0:
			tiempo = "0"+str(minutos)+":00"
			timeispositive = false
	else:
		if segundos <= 9:
			tiempo = "0"+str(minutos)+":0"+str(segundos)
			segundos = segundos + 1
		if segundos > 9 and segundos < 59:
			tiempo = "0"+str(minutos)+":"+str(segundos)
			segundos = segundos + 1
		if segundos == 59:
			tiempo = "0"+str(minutos)+":"+str(segundos)
			segundos = 00
			minutos = minutos + 1
