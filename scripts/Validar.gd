extends TextureButton

onready var numeroprevio =  get_node("/root/MusicScene").mylist[0]
onready var presionados = get_node("/root/MusicScene").presionados
onready var attempt = 1

func _ready():
	$ImagenQueQuiero.set_texture(preload("res://assets/selected.png"))
	$BinaryCode/Label.set_text(str(get_node("/root/MusicScene").binarycode))
	for i in len(presionados):
		get_parent().get_node("Fichas/{}".replace("{}", presionados[i])).set_pressed(true)

func _on_0_pressed():
	$ImagenQueQuiero.set_texture(preload("res://assets/0.png"))
	get_node("/root/MusicScene").funcionseleccionada = 0

func _on_1_pressed():
	$ImagenQueQuiero.set_texture(preload("res://assets/1.png"))
	get_node("/root/MusicScene").funcionseleccionada  = 1

func _on_3_pressed():
	$ImagenQueQuiero.set_texture(preload("res://assets/36.png"))
	get_node("/root/MusicScene").funcionseleccionada  = 3

func _on_4_pressed():
	$ImagenQueQuiero.set_texture(preload("res://assets/4.png"))
	get_node("/root/MusicScene").funcionseleccionada  = 4

func _on_6_pressed():
	$ImagenQueQuiero.set_texture(preload("res://assets/6.png"))
	get_node("/root/MusicScene").funcionseleccionada  = 6

func _on_8_pressed():
	$ImagenQueQuiero.set_texture(preload("res://assets/8.png"))
	get_node("/root/MusicScene").funcionseleccionada  = 8

func _on_9_pressed():
	$ImagenQueQuiero.set_texture(preload("res://assets/9.png"))
	get_node("/root/MusicScene").funcionseleccionada  = 9

func _on_12_pressed():
	$ImagenQueQuiero.set_texture(preload("res://assets/12.png"))
	get_node("/root/MusicScene").funcionseleccionada  = 12

func _on_14_pressed():
	$ImagenQueQuiero.set_texture(preload("res://assets/14.png"))
	get_node("/root/MusicScene").funcionseleccionada = 14

func _on_16_pressed():
	$ImagenQueQuiero.set_texture(preload("res://assets/16.png"))
	get_node("/root/MusicScene").funcionseleccionada  = 16

func _on_20_pressed():
	$ImagenQueQuiero.set_texture(preload("res://assets/20.png"))
	get_node("/root/MusicScene").funcionseleccionada  = 20

func _on_22_pressed():
	$ImagenQueQuiero.set_texture(preload("res://assets/22.png"))
	get_node("/root/MusicScene").funcionseleccionada  = 22

func _on_24_pressed():
	$ImagenQueQuiero.set_texture(preload("res://assets/24.png"))
	get_node("/root/MusicScene").funcionseleccionada  = 24

func _on_28_pressed():
	$ImagenQueQuiero.set_texture(preload("res://assets/28.png"))
	get_node("/root/MusicScene").funcionseleccionada  = 28

func _on_30_pressed():
	$ImagenQueQuiero.set_texture(preload("res://assets/30.png"))
	get_node("/root/MusicScene").funcionseleccionada  = 30 

func _on_32_pressed():
	$ImagenQueQuiero.set_texture(preload("res://assets/32.png"))
	get_node("/root/MusicScene").funcionseleccionada  = 32

func _on_33_pressed():
	$ImagenQueQuiero.set_texture(preload("res://assets/33.png"))
	get_node("/root/MusicScene").funcionseleccionada  = 33

func _on_38_pressed():
	$ImagenQueQuiero.set_texture(preload("res://assets/38.png"))
	get_node("/root/MusicScene").funcionseleccionada  = 38

func _on_40_pressed():
	$ImagenQueQuiero.set_texture(preload("res://assets/40.png"))
	get_node("/root/MusicScene").funcionseleccionada  = 40

func _on_41_pressed():
	$ImagenQueQuiero.set_texture(preload("res://assets/41.png"))
	get_node("/root/MusicScene").funcionseleccionada  = 41

func _on_44_pressed():
	$ImagenQueQuiero.set_texture(preload("res://assets/44.png"))
	get_node("/root/MusicScene").funcionseleccionada  = 44

func _on_46_pressed():
	$ImagenQueQuiero.set_texture(preload("res://assets/46.png"))
	get_node("/root/MusicScene").funcionseleccionada  = 46

func _on_48_pressed():
	$ImagenQueQuiero.set_texture(preload("res://assets/48.png"))
	get_node("/root/MusicScene").funcionseleccionada  = 48

func _on_52_pressed():
	$ImagenQueQuiero.set_texture(preload("res://assets/52.png"))
	get_node("/root/MusicScene").funcionseleccionada  = 52

func _on_54_pressed():
	$ImagenQueQuiero.set_texture(preload("res://assets/54.png"))
	get_node("/root/MusicScene").funcionseleccionada  = 54

func _on_56_pressed():
	$ImagenQueQuiero.set_texture(preload("res://assets/56.png"))
	get_node("/root/MusicScene").funcionseleccionada  = 56

func _on_60_pressed():
	$ImagenQueQuiero.set_texture(preload("res://assets/60.png"))
	get_node("/root/MusicScene").funcionseleccionada  = 60

func _on_62_pressed():
	$ImagenQueQuiero.set_texture(preload("res://assets/62.png"))
	get_node("/root/MusicScene").funcionseleccionada  = 62 

func _on_Validar_pressed():
	var img = str(get_node("/root/MusicScene").funcionseleccionada)
	var num = $NumeroEscrito.get_text()
	if num == str(numeroprevio) and img == str(numeroprevio) and attempt == 1:
		get_node("/root/MusicScene").PuntosPorAttempts= get_node("/root/MusicScene").PuntosPorAttempts+50
		get_tree().change_scene("res://Puntos.tscn")
	elif num == str(numeroprevio) and img == str(numeroprevio) and attempt > 1:
		get_tree().change_scene("res://Puntos.tscn")
	elif num == str(numeroprevio) and img != str(numeroprevio):
		get_node("/root/MusicScene").PuntosPorAttempts= get_node("/root/MusicScene").PuntosPorAttempts- 10
		attempt += 1
		get_parent().get_node("Menú/DialogBox/Control").textanimation(get_parent().get_node("Menú/DialogBox/Control").dialog,4)
	elif num != str(numeroprevio) and img == str(numeroprevio):
		get_node("/root/MusicScene").PuntosPorAttempts= get_node("/root/MusicScene").PuntosPorAttempts- 10
		attempt += 1
		get_parent().get_node("Menú/DialogBox/Control").textanimation(get_parent().get_node("Menú/DialogBox/Control").dialog,5)
	else:
		get_node("/root/MusicScene").PuntosPorAttempts= get_node("/root/MusicScene").PuntosPorAttempts- 15
		attempt += 1
		get_parent().get_node("Menú/DialogBox/Control").textanimation(get_parent().get_node("Menú/DialogBox/Control").dialog,6)

func _on_BacktoStart_pressed():
	get_tree().change_scene("res://Main Scene.tscn")
