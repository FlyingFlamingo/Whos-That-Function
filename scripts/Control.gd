extends Control

var dialog = ["¡Has llegado al tutorial para 'Who's that Function!",\
			  "El uso es muy sencillo: se trata del clásico '¿Quién es Quién?', pero, ¡con funciones!",\
			  "Al principio del juego, yo seleccionaré una función, y tú tendrás que adivinar cuál es haciéndome preguntas",\
			  "Nuestro lenguaje va a ser binario, así que, si te respondo que Sí, traslada un 1 a la casilla correspondiente; si digo que No, deberás colocar un 0",\
			  "¡Mira el Vídeo para entender cómo!",\
			  "Esta es la línea 6, y el programa no la usa",\
			  "Una vez seleccionado, debes clicar en 'Siguiente', tras lo que aparecerá una nueva sección en la que podrás validar tu elección",\
			  "Simplemente selecciona la función adecuada, introduce su código decimal y pulsa 'Validar'",\
			  "¡Puedes ver el vídeo para entenderlo mejor!",\
			  "Pero, ¿Cómo sabrás si eres un Pitagorín o si aún te queda mucho por aprender, pequeño saltamontes?",\
			  "Muy fácil: El juego tiene un sistema de puntos que funciona de la siguiente manera:",\
			  "Al final del juego, calcularé los puntos totales que te mereces y te asignaré un rango.",\
			  "¿Estás listo para jugar? ¡Mucha suerte!",\
			  "WoW! 42 veces has clicado! Tu evidente abuerrimiento te acaba de descubir La Respuesta a la Vida, el Universo y Todo lo Demás"]

var page = 0
var video1hasplayed = false
var video2hasplayed = false

func _process(delta):
	if Input.is_action_just_pressed("ui_accept"):
		dialogo()

func _ready():
	get_parent().get_node("Media/PreIMG1").visible = true
	get_parent().get_node("Media/PrimerVideo").visible = false
	get_parent().get_node("Media/PreIMG2").visible = false
	get_parent().get_node("Media/PreIMG3").visible = false
	get_parent().get_node("Media/SegundoVideo").visible = false
	get_parent().get_node("Media/Puntos").visible = false
	dialogo()

func textanimation(dialog, page):
	$Indicador.visible = false
	$DialogText.bbcode_text = dialog[page]
	$DialogText.percent_visible = 0
	$TextAnimation.interpolate_property(
		$DialogText, "percent_visible", 0, 1, 1,
		Tween.TRANS_LINEAR, Tween.EASE_IN_OUT
	)
	$TextAnimation.start()

func dialogo():
	if page < 5:
		textanimation(dialog, page)
		page += 1
	elif page == 5:
		$Indicador.visible = false
		get_parent().get_node("Media/PreIMG1").visible = false
		get_parent().get_node("Media/PrimerVideo").visible = true
		get_parent().get_node("Media/PrimerVideo").play()
		page += 1
	elif page == 6 and video1hasplayed == true:
		get_parent().get_node("Media/PrimerVideo").visible = false
		textanimation(dialog, page)
		get_parent().get_node("Media/PreIMG2").visible = true
		page += 1
	elif page == 7:
		get_parent().get_node("Media/PreIMG2").visible = false
		textanimation(dialog, page)
		get_parent().get_node("Media/PreIMG3").visible = true
		page += 1
	elif page == 8:
		textanimation(dialog, page)
		$Indicador.visible = false
		get_parent().get_node("Media/PreIMG3").visible = false
		get_parent().get_node("Media/SegundoVideo").visible = true
		get_parent().get_node("Media/SegundoVideo").play()
		page += 1
	elif page == 9 and video2hasplayed == true:
		textanimation(dialog, page)
		page += 1
	elif page == 10 and video2hasplayed == true:
		textanimation(dialog, page)
		get_parent().get_node("Media/SegundoVideo").visible = false
		get_parent().get_node("Media/Puntos").visible = true
		page += 1
	elif page > 10 and page < 42 and video2hasplayed == true:
		textanimation(dialog, 12)
		page += 1
		get_parent().get_node("Media/Puntos").visible = false
		get_parent().get_node("Botones/GoBackToStart").visible = false
		get_parent().get_node("Botones/HiddenBackToStart").visible = true
		get_parent().get_node("Botones/HiddenLetsPlay").visible = true
		get_parent().get_node("Botones/Music").visible = false
	elif page == 42: 
		textanimation(dialog, 13)
		page += 1
	elif page > 42: 
		textanimation(dialog, 12)
	
func _on_TextAnimation_tween_completed(object, key):
	$Indicador.visible = true

func _on_PrimerVideo_finished():
	$Indicador.visible = true
	video1hasplayed = true

func _on_SegundoVideo_finished():
	$Indicador.visible = true
	video2hasplayed = true

func _on_GoBackToStart_pressed():
	get_tree().change_scene("res://Splash Screen.tscn")

func _on_HiddenLetsPlay_pressed():
	get_node("/root/MusicScene").minutos = 9; get_node("/root/MusicScene").segundos = 59
	get_tree().change_scene("res://Main Scene.tscn")

func _on_Music_pressed():
	if get_node("/root/MusicScene/Music").is_playing():
		MusicScene.stop_music()
	else:
		MusicScene.play_music()

func _on_HiddenBackToStart_pressed():
	get_tree().change_scene("res://Splash Screen.tscn")
