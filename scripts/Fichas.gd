extends GridContainer

onready var presionados = get_node("/root/MusicScene").presionados

func _ready():
	for i in len(presionados):
		get_parent().get_node("Fichas/{}".replace("{}", presionados[i])).set_pressed(true)

func _on_0_toggled(button_pressed):
	if get_node("0").is_pressed():
		presionados.append(0)
	else:
		presionados.erase(0)
		presionados.erase(0)

func _on_1_toggled(button_pressed):
	if get_node("1").is_pressed():
		presionados.append(1)
	else:
		presionados.erase(1)
		presionados.erase(1)

func _on_3_toggled(button_pressed):
	if get_node("3").is_pressed():
		presionados.append(3)
	else:
		presionados.erase(3)
		presionados.erase(3)

func _on_4_toggled(button_pressed):
	if get_node("4").is_pressed():
		presionados.append(4)
	else:
		presionados.erase(4)
		presionados.erase(4)

func _on_6_toggled(button_pressed):
	if get_node("6").is_pressed():
		presionados.append(6)
	else:
		presionados.erase(6)
		presionados.erase(6)

func _on_8_pressed():
	if get_node("8").is_pressed():
		presionados.append(8)
	else:
		presionados.erase(8)
		presionados.erase(8)

func _on_9_pressed():
	if get_node("9").is_pressed():
		presionados.append(9)
	else:
		presionados.erase(9)
		presionados.erase(9)

func _on_12_toggled(button_pressed):
	if get_node("12").is_pressed():
		presionados.append(12)
	else:
		presionados.erase(12)
		presionados.erase(12)

func _on_14_toggled(button_pressed):
	if get_node("14").is_pressed():
		presionados.append(14)
	else:
		presionados.erase(14)
		presionados.erase(14)

func _on_16_toggled(button_pressed):
	if get_node("16").is_pressed():
		presionados.append(16)
	else:
		presionados.erase(16)
		presionados.erase(16)

func _on_20_toggled(button_pressed):
	if get_node("20").is_pressed():
		presionados.append(20)
	else:
		presionados.erase(20)
		presionados.erase(20)

func _on_22_toggled(button_pressed):
	if get_node("22").is_pressed():
		presionados.append(22)
	else:
		presionados.erase(22)
		presionados.erase(22)

func _on_24_toggled(button_pressed):
	if get_node("24").is_pressed():
		presionados.append(24)
	else:
		presionados.erase(24)
		presionados.erase(24)

func _on_28_toggled(button_pressed):
	if get_node("28").is_pressed():
		presionados.append(28)
	else:
		presionados.erase(28)
		presionados.erase(28)

func _on_30_toggled(button_pressed):
	if get_node("30").is_pressed():
		presionados.append(30)
	else:
		presionados.erase(30)
		presionados.erase(30)

func _on_32_toggled(button_pressed):
	if get_node("32").is_pressed():
		presionados.append(32)
	else:
		presionados.erase(32)
		presionados.erase(32)


func _on_33_toggled(button_pressed):
	if get_node("33").is_pressed():
		presionados.append(33)
	else:
		presionados.erase(33)
		presionados.erase(33)

func _on_38_toggled(button_pressed):
	if get_node("38").is_pressed():
		presionados.append(38)
	else:
		presionados.erase(38)
		presionados.erase(38)

func _on_40_toggled(button_pressed):
	if get_node("40").is_pressed():
		presionados.append(40)
	else:
		presionados.erase(40)
		presionados.erase(40)

func _on_41_toggled(button_pressed):
	if get_node("41").is_pressed():
		presionados.append(41)
	else:
		presionados.erase(41)
		presionados.erase(41)

func _on_44_toggled(button_pressed):
	if get_node("44").is_pressed():
		presionados.append(44)
	else:
		presionados.erase(44)
		presionados.erase(44)

func _on_46_toggled(button_pressed):
	if get_node("46").is_pressed():
		presionados.append(46)
	else:
		presionados.erase(46)
		presionados.erase(46)

func _on_48_toggled(button_pressed):
	if get_node("48").is_pressed():
		presionados.append(48)
	else:
		presionados.erase(48)
		presionados.erase(48)

func _on_52_toggled(button_pressed):
	if get_node("52").is_pressed():
		presionados.append(52)
	else:
		presionados.erase(52)
		presionados.erase(52)

func _on_54_toggled(button_pressed):
	if get_node("54").is_pressed():
		presionados.append(54)
	else:
		presionados.erase(54)
		presionados.erase(54)

func _on_56_toggled(button_pressed):
	if get_node("56").is_pressed():
		presionados.append(56)
	else:
		presionados.erase(56)
		presionados.erase(56)

func _on_60_toggled(button_pressed):
	if get_node("60").is_pressed():
		presionados.append(60)
	else:
		presionados.erase(60)
		presionados.erase(60)
		
func _on_62_toggled(button_pressed):
	if get_node("62").is_pressed():
		presionados.append(62)
	else:
		presionados.erase(62)
		presionados.erase(62)

func _on_Siguiente_pressed():
	var NegID = get_parent().get_node("Preguntas/Answer/SaveNeg").get_selected_id()
	var AcoID = get_parent().get_node("Preguntas/Answer2/SaveAco").get_selected_id()
	var MonID = get_parent().get_node("Preguntas/Answer3/SaveMon").get_selected_id()
	var ContID = get_parent().get_node("Preguntas/Answer4/SaveCont").get_selected_id()
	var DerID = get_parent().get_node("Preguntas/Answer5/SaveDer").get_selected_id()
	var AVID = get_parent().get_node("Preguntas/Answer6/SaveAV").get_selected_id()
	var binarycode = str(NegID)+str(AcoID)+str(MonID)+str(ContID)+str(DerID)+str(AVID)
	binarycode = binarycode.replace("0","?")
	binarycode = binarycode.replace("1","0")
	binarycode = binarycode.replace("2","1")
	get_node("/root/MusicScene").binarycode = binarycode
	get_tree().change_scene("res://Seleccionar.tscn")

func _on_BacktoStart_pressed():
	#Reloading autoload values
	get_parent().get_node("/root/MusicScene").presionados = [] #Usada para contar qué funciones he clickado
	get_parent().get_node("/root/MusicScene").funcionseleccionada #Función seleccionada para validar
	get_parent().get_node("/root/MusicScene").binarycode #Variable que indica el código respuesta de las preguntas
	get_parent().get_node("/root/MusicScene").preguntassinconsultar = 6 #Contador de puntos
	get_parent().get_node("/root/MusicScene").PuntosPorAttempts = 0 #Usado en la escena Seleccionar
	get_parent().get_node("/root/MusicScene").tiempo = "10:00"  #Contador para el tiempo total
	get_parent().get_node("/root/MusicScene").minutos = 10
	get_parent().get_node("/root/MusicScene").segundos = 00
	get_parent().get_node("/root/MusicScene").timeispositive = true
	randomize()
	var one_number_to_rule_them_all = randi()%28+1
	get_parent().get_node("/root/MusicScene").mylist = get_parent().get_node("/root/MusicScene").csv2Dict(one_number_to_rule_them_all)
	get_tree().change_scene("res://Splash Screen.tscn")
