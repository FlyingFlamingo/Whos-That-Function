extends GridContainer

func _ready():
	$Answer/SaveNeg.add_item("?"); $Answer/SaveNeg.add_item("0"); $Answer/SaveNeg.add_item("1")
	$Answer2/SaveAco.add_item("?"); $Answer2/SaveAco.add_item("0"); $Answer2/SaveAco.add_item("1")
	$Answer3/SaveMon.add_item("?"); $Answer3/SaveMon.add_item("0"); $Answer3/SaveMon.add_item("1")
	$Answer4/SaveCont.add_item("?"); $Answer4/SaveCont.add_item("0"); $Answer4/SaveCont.add_item("1")
	$Answer5/SaveDer.add_item("?"); $Answer5/SaveDer.add_item("0"); $Answer5/SaveDer.add_item("1")
	$Answer6/SaveAV.add_item("?"); $Answer6/SaveAV.add_item("0"); $Answer6/SaveAV.add_item("1")

func _on_Negativa_pressed():
	$Answer/Label.set_text(get_node("/root/MusicScene").mylist[1])
	get_node("/root/MusicScene").preguntassinconsultar = get_node("/root/MusicScene").preguntassinconsultar - 1
	$Answer/Label.show()
	
func _on_Acotada_pressed():
	$Answer2/Label.set_text(get_node("/root/MusicScene").mylist[2])
	get_node("/root/MusicScene").preguntassinconsultar = get_node("/root/MusicScene").preguntassinconsultar - 1
	$Answer/Label.show()

func _on_Montona_pressed():
	$Answer3/Label.set_text(get_node("/root/MusicScene").mylist[3])
	get_node("/root/MusicScene").preguntassinconsultar = get_node("/root/MusicScene").preguntassinconsultar - 1
	$Answer3/Label.show()

func _on_Continua_pressed():
	$Answer4/Label.set_text(get_node("/root/MusicScene").mylist[4])
	get_node("/root/MusicScene").preguntassinconsultar = get_node("/root/MusicScene").preguntassinconsultar - 1
	$Answer4/Label.show()

func _on_Derivable_pressed():
	$Answer5/Label.set_text(get_node("/root/MusicScene").mylist[5])
	get_node("/root/MusicScene").preguntassinconsultar = get_node("/root/MusicScene").preguntassinconsultar - 1
	$Answer5/Label.show()

func _on_AV_pressed():
	$Answer6/Label.set_text(get_node("/root/MusicScene").mylist[6])
	get_node("/root/MusicScene").preguntassinconsultar = get_node("/root/MusicScene").preguntassinconsultar - 1
	$Answer6/Label.show()
