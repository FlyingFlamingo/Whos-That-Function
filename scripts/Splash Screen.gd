extends TextureRect

func _ready():
	MusicScene.play_music()

func _on_Start_pressed():
	get_node("/root/MusicScene").minutos = 9; get_node("/root/MusicScene").segundos = 59
	get_tree().change_scene("res://Main Scene.tscn")

func _on_Tutorial_pressed():
	get_tree().change_scene("res://Tutorial.tscn")

func _on_Crditos_pressed():
	get_tree().change_scene("res://Creditos.tscn")

func _on_BootStrap_meta_clicked(meta):
	OS.shell_open("https://icons.getbootstrap.com/")

func _on_Font_meta_clicked(meta):
	OS.shell_open("https://fontlibrary.org/en/font/cooper-hewitt")

func _on_Music_meta_clicked(meta):
	OS.shell_open("https://open.audio/library/tracks/39141/")

func _on_Fondo_meta_clicked(meta):
	OS.shell_open("https://www.freepik.com/free-vector/abstract-colorful-flow-shapes-background_5226074.htm#page=1&query=background&position=12")

func _on_Laurel_meta_clicked(meta):
	OS.shell_open("https://commons.wikimedia.org/wiki/File:Greek_Roman_Laurel_wreath_vector.svg")

func _on_Trofeo_meta_clicked(meta):
	OS.shell_open("https://commons.wikimedia.org/wiki/File:Circle-icons-trophy_(dark).svg")

func _on_Libro_meta_clicked(meta):
	OS.shell_open("https://commons.wikimedia.org/wiki/File:Book_Hexagonal_Icon.svg")

func _on_Source_Code_meta_clicked(meta):
	OS.shell_open("https://codeberg.org/FlyingFlamingo/Whos-That-Function")

func _on_HiddenBackToStart_pressed():
	get_tree().change_scene("res://Splash Screen.tscn")


func _on_Next_pressed():
	get_node("Externals").visible = true
	get_node("Internals").visible = false
	get_node("Next").visible = false

func _on_IntroText_meta_clicked(meta):
	OS.shell_open("http://innovacioneducativa.upm.es/pensamientomatematico/")

func _on_Maril_meta_clicked(meta):
	OS.shell_open("mailto:marilo.lopez@upm.es ")

func _on_Sagrario_meta_clicked(meta):
	OS.shell_open("mailto:sagrario.lantaron@upm.es")

func _on_Susana_meta_clicked(meta):
	OS.shell_open("mailto:susana.mercha@upm.es  ")

func _on_Javier_meta_clicked(meta):
	OS.shell_open("mailto:jrodrigo@icai.comillas.edu ")

func _on_Yo_meta_clicked(meta):
	OS.shell_open("mailto:pablo.marcos.lopez@alumnos.upm.es ")

func _on_CC_pressed():
	OS.shell_open("http://creativecommons.org/licenses/by-nc-nd/3.0/deed.es_ES")

func _on_Codeberg_pressed():
	OS.shell_open("https://codeberg.org/FlyingFlamingo/Whos-That-Function")

func _on_AGPL_pressed():
	OS.shell_open("https://www.gnu.org/licenses/agpl-3.0.txt")


func _on_Music_pressed():
	if get_node("/root/MusicScene/Music").is_playing():
		MusicScene.stop_music()
	else:
		MusicScene.play_music()

func _on_Godot_meta_clicked(meta):
	OS.shell_open("https://godotengine.org/license")
