extends Control

var dialog = ["¡Es el momento de la verdad! ¿Habrás acertado?",\
			  "Selecciona la función que has escogido en la lista e introduce su código en DECIMAL",\
			  "", "",\
			  "ERROR: Has introducido mal la ficha de la función. Por favor, inténtalo de nuevo",\
			  "ERROR: Has introducido mal el código de la función. Por favor, inténtalo de nuevo",\
			  "ERROR: Has introducido mal tanto el código de la función como su ficha. Por favor, inténtalo de nuevo"]

var page = 0

func _ready():
	textanimation(dialog, page)
	page += 1

func _process(delta):
	if Input.is_action_just_pressed("ui_accept"):
		if page < 2:
			textanimation(dialog, page)
			page += 1
		if page == 2:
			$Indicador.visible = false

func textanimation(dialog, page):
	$Indicador.visible = false
	$DialogText.bbcode_text = dialog[page]
	$DialogText.percent_visible = 0
	$TextAnimation.interpolate_property(
		$DialogText, "percent_visible", 0, 1, 1,
		Tween.TRANS_LINEAR, Tween.EASE_IN_OUT
	)
	$TextAnimation.start()

func _on_TextAnimation_tween_completed(object, key):
	$Indicador.visible = true
