var dialog = ["¡Bienvenido al tutorial para el juego 'Who's that Function!",\
			  "El uso es muy sencillo: se trata del clásico '¿Quién es Quién?', pero, ¡con funciones!",\
			  "Al principio del juego, yo seleccionaré una función, y tu tendrás que adivinar cuál es haciéndome preguntas",\
			  "Nuestro lenguaje va a ser binario, así que, si te respondo que Sí, traslada un 1 a la casilla correspondiente; si digo que no, deberás colocar un 0",\
			  "¡Mira el Vídeo para entender cómo!"]

var page = 0

func _ready():
	if page < dialog.size():
		get_parent().get_node("Indicador").visible = false
		$DialogText.bbcode_text = dialog[page]
		$DialogText.percent_visible = 0
		$TextAnimation.interpolate_property(
			$DialogText, "percent_visible", 0, 1, 1,
			Tween.TRANS_LINEAR, Tween.EASE_IN_OUT
		)
		$TextAnimation.start()
	else:
		queue_free()
	page += 1

func _on_TextAnimation_tween_completed(object, key):
	get_parent().get_node("Indicador").visible = true
	pass
