extends Control

var dialog = ["¡Vamos allá! ¿List@ para jugar?",\
			  "Vete haciéndome las preguntas que consideres. No olvides ir tachando funciones y trasladar la respuesta a cada pregunta en binario",\
			  "Para esto último, puedes usar los dropdowns en el menú de abajo",\
			  "¡Mucha suerte!"]
var page = 0

func _ready():
	textanimation(dialog, page)
	page += 1

func _process(delta):
	if Input.is_action_just_pressed("ui_accept"):
		if page < dialog.size():
			textanimation(dialog, page)
			page += 1
		if page == dialog.size():
			$Indicador.visible = false

func textanimation(dialog, page):
	$Indicador.visible = false
	$DialogText.bbcode_text = dialog[page]
	$DialogText.percent_visible = 0
	$TextAnimation.interpolate_property(
		$DialogText, "percent_visible", 0, 1, 1,
		Tween.TRANS_LINEAR, Tween.EASE_IN_OUT
	)
	$TextAnimation.start()

func _on_TextAnimation_tween_completed(object, key):
	$Indicador.visible = true
