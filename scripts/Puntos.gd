extends TextureRect

onready var PuntosPorPreg = get_node("/root/MusicScene").preguntassinconsultar * 50
onready var PuntosPorTiempo = get_time_points()
onready var PuntosPorAttempts = get_node("/root/MusicScene").PuntosPorAttempts
onready var Puntos = PuntosPorPreg + PuntosPorTiempo + PuntosPorAttempts + 100

onready var dialog = ["",\
			  "Preguntas sin consultar: 	{0}. 		A 75 puntos cada una, {0} * 75  	= 	{1} Puntos".format({"0":get_node("/root/MusicScene").preguntassinconsultar,"1":PuntosPorPreg}),\
			  "Bonificación por tiempo 																				  :		{0} Puntos".format({"0":str(PuntosPorTiempo)}),\
			  "Bonificación/penalización por intentos 													  :			{0} Puntos".format({"0":str(PuntosPorAttempts)}),\
			  "Lo que suma un total de 																				  :		{0} Puntos".format({"0":str(Puntos)}),\
			  " 										Por lo que has conseguido el Rango...", ""]
			
var mypage = 1

func _process(delta):
	if Input.is_action_just_pressed("ui_accept"):
			dialogo()

func dialogo():
	if mypage == 1:
		textanimation("Puntos/PregSinConsult/Label", dialog, mypage)
		$Control/Indicador.visible = false
	if mypage == 2:
		textanimation("Puntos/Tiempo/Label", dialog, mypage)
		$Control/Indicador.visible = false
	if mypage == 3:
		textanimation("Puntos/Attempts/Label", dialog, mypage)
		$Control/Indicador.visible = false
	if mypage == 4:
		textanimation("Puntos/ElKiloLeSaleA/Label", dialog, mypage)
		$Control/Indicador.visible = false
	if mypage == 5:
		textanimation("Puntos/YTuRangoEs/Label", dialog, mypage)
		$Control/Indicador.visible = false
	if mypage == 6:
		get_node("Rangos").visible = true
		if Puntos > 1000:
			get_node("Rangos/A").set_modulate(Color(1,1,1,1))
		if Puntos < 1000 and Puntos >= 750:
			get_node("Rangos/B").set_modulate(Color(1,1,1,1))
		if Puntos < 750 and Puntos >= 500:
			get_node("Rangos/C").set_modulate(Color(1,1,1,1))
		if Puntos < 500 and Puntos >= 250:
			get_node("Rangos/D").set_modulate(Color(1,1,1,1))
		if Puntos < 250:
			get_node("Rangos/E").set_modulate(Color(1,1,1,1))
		get_node("Rangos").visible = true
		textanimation("Puntos/DummyLabel", dialog, mypage)
		get_node("HiddenBackToStart").visible = true
		$Control/Indicador.visible = false

func textanimation(texttoanimate, dialog, mypage):
	get_node(texttoanimate).bbcode_text = dialog[mypage]
	get_node(texttoanimate).percent_visible = 0
	$Puntos/Animation.interpolate_property(
		get_node(texttoanimate), "percent_visible", 0, 1, 1,
		Tween.TRANS_LINEAR, Tween.EASE_IN_OUT
	)
	$Puntos/Animation.start()

func get_time_points():
	var minutos = get_node("/root/MusicScene").minutos
	var segundos = get_node("/root/MusicScene").segundos
	var foo
	if segundos > 30:
		foo = minutos*2*50 + 50
	else:
		foo = minutos*2*50
	if  get_node("/root/MusicScene").timeispositive == true:
		return(foo)
	else:
		return(-foo)

func _on_Animation_tween_started(object, key):
	mypage +=1

func _on_HiddenBackToStart_pressed():
	#Reloading autoload values
	get_parent().get_node("/root/MusicScene").presionados = [] #Usada para contar qué funciones he clickado
	get_parent().get_node("/root/MusicScene").funcionseleccionada #Función seleccionada para validar
	get_parent().get_node("/root/MusicScene").binarycode #Variable que indica el código respuesta de las preguntas
	get_parent().get_node("/root/MusicScene").preguntassinconsultar = 6 #Contador de puntos
	get_parent().get_node("/root/MusicScene").PuntosPorAttempts = 0 #Usado en la escena Seleccionar
	get_parent().get_node("/root/MusicScene").tiempo = "10:00"  #Contador para el tiempo total
	get_parent().get_node("/root/MusicScene").minutos = 10
	get_parent().get_node("/root/MusicScene").segundos = 00
	get_parent().get_node("/root/MusicScene").timeispositive = true
	randomize()
	var one_number_to_rule_them_all = randi()%28+1
	get_parent().get_node("/root/MusicScene").mylist = get_parent().get_node("/root/MusicScene").csv2Dict(one_number_to_rule_them_all)
	get_tree().change_scene("res://Splash Screen.tscn")


func _on_Animation_tween_completed(object, key):
	if mypage <= 6:
		$Control/Indicador.visible = true
	if mypage > 6:
		pass
