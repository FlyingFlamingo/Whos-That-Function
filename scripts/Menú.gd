extends HBoxContainer

func _process(delta):
	if get_node("/root/MusicScene").timeispositive == false:
		$Timer.add_color_override("font_color", Color(0.8, 0.16, 0.1))
		$Timer.set_text(get_node("/root/MusicScene").tiempo)
	else:
		$Timer.set_text(get_node("/root/MusicScene").tiempo)

func _on_Music_pressed():
	if get_node("/root/MusicScene/Music").is_playing():
		MusicScene.stop_music()
	else:
		MusicScene.play_music()

func _on_Info_pressed():
	$Info/Info.visible = true
	$Info/GoOut.visible = true
	$Music.visible = false
	get_tree().paused = true

func _on_GoOut_pressed():
	$Info/Info.visible = false
	$Info/GoOut.visible = false
	$Music.visible = true
	get_tree().paused = false
