extends Control

var dialog = ["Bueno bueno bueno... Veamos cuantos puntos has obtenido!!"]

var page = 0

func _ready():
	textanimation(dialog, page)

func textanimation(dialog, page):
	$Indicador.visible = false
	$DialogText.bbcode_text = dialog[page]
	$DialogText.percent_visible = 0
	$TextAnimation.interpolate_property(
		$DialogText, "percent_visible", 0, 1, 1,
		Tween.TRANS_LINEAR, Tween.EASE_IN_OUT
	)
	$TextAnimation.start()

func _on_TextAnimation_tween_completed(object, key):
	$Indicador.visible = true
	page = 2
