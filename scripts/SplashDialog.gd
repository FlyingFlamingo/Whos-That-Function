extends Control

var dialog = ["¡Bienvenido al juego 'Who's that Function! Para comenzar, pulsa en cualquier parte del juego; después, puedes usar la barra espaciadora para continuar",\
			  "¿Quieres jugar al Quién es Quién... pero con funciones?",\
			  "¡Yo ya he elegido mi función!, ¿Podrás adivinarla?",\
			   "Antes de jugar, recuerda que puedes activar el modo pantalla completa con el botón más abajo",\
			   "¡Mucha suerte!"]
var page = 0

func _ready():
	textanimation(dialog, page)
	page += 1

func _process(delta):
	if Input.is_action_just_pressed("ui_accept"):
		if page < dialog.size():
			textanimation(dialog, page)
			page += 1
		if page == dialog.size():
			$Indicador.visible = false


func textanimation(dialog, page):
	$Indicador.visible = false
	$DialogText.bbcode_text = dialog[page]
	$DialogText.percent_visible = 0
	$TextAnimation.interpolate_property(
		$DialogText, "percent_visible", 0, 1, 1,
		Tween.TRANS_LINEAR, Tween.EASE_IN_OUT
	)
	$TextAnimation.start()

func _on_TextAnimation_tween_completed(object, key):
	$Indicador.visible = true
